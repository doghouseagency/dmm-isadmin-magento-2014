<?php

/**
 * http://stackoverflow.com/questions/9693020/magento-request-frontend-or-backend
 *
 * Thanks Alan.
 *
 */
class Doghouse_IsAdmin_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isAdmin()
    {
        if(Mage::app()->getStore()->isAdmin())
        {
            return true;
        }

        if(Mage::getDesign()->getArea() == 'adminhtml')
        {
            return true;
        }

        return false;
    }

    public function isFrontend()
    {
        return !$this->isAdmin();
    }
}