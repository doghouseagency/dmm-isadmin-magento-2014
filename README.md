# Is Admin

Programmatically check if you're in admin or frontend:

	Mage::helper('doghouse_isadmin')->isAdmin()

and

	Mage::helper('doghouse_isadmin')->isFrontend()

